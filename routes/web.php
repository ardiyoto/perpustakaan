<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return view('auth.login');
});

Route::group(['middleware' => ['isAdmin']], function() {
       

    // Anggota
    Route::get('/anggota/add','AnggotaControl@add');
    Route::post('/anggota/save','AnggotaControl@save');
    Route::get('/anggota/delete/{id}','AnggotaControl@delete');
    Route::get('/anggota/edit/{id}','AnggotaControl@edit');

    // Buku
    Route::get('/buku/add','BukuControl@add');
    Route::post('/buku/save','BukuControl@save');
    Route::get('/buku/delete/{id}','BukuControl@delete');
    Route::get('/buku/edit/{id}','BukuControl@edit');

    // Koleksi
    Route::get('/koleksi/add','KoleksiControl@add');
    Route::post('/koleksi/save','KoleksiControl@save');
    Route::get('/koleksi/delete/{id}','KoleksiControl@delete');
    Route::get('/koleksi/edit/{id}','KoleksiControl@edit');

    // Transaksi
    Route::get('/trans/peminjaman','TransaksiControl@peminjaman');
    Route::post('/trans/peminjaman','TransaksiControl@peminjaman');
    Route::post('/trans/peminjaman/save','TransaksiControl@save');

    Route::get('report/qrcode_buku','ReportControl@QR_Code_Buku');
    Route::get('report/qrcode_anggota','ReportControl@QR_Code_Anggota');

    Route::get('report/cetak','ReportControl@cetak');
    Route::get('report/cetak_anggota','ReportControl@cetak_anggota');

    // your routes
    Route::get('user','UsersControl@index');
    Route::get('user/add','UsersControl@add');
    Route::get('user/edit/{id}','UsersControl@edit');
    Route::post('user/save','UsersControl@save');

});

Route::group(['middleware' => ['isOperator']], function() {
    // Anggota
    Route::get('/anggota','AnggotaControl@index');
    // Buku
    Route::get('/buku','BukuControl@index');
    // Koleksi
    Route::get('/koleksi','KoleksiControl@index');   
    // Transaksi
    Route::get('/trans/peminjaman','TransaksiControl@peminjaman');
    Route::post('/trans/peminjaman','TransaksiControl@peminjaman');
    Route::post('/trans/peminjaman/save','TransaksiControl@save');

    Route::post('/trans/pengembalian','TransaksiControl@pengembalian');
    Route::get('/trans/pengembalian','TransaksiControl@pengembalian');
    Route::post('/trans/pengembalian/save','TransaksiControl@save_pengembalian');

    Route::get('report/qrcode_buku','ReportControl@QR_Code_Buku');
    Route::get('report/qrcode_anggota','ReportControl@QR_Code_Anggota');

    Route::get('report/cetak','ReportControl@cetak');
    Route::get('report/cetak_anggota','ReportControl@cetak_anggota');    
});


Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');

// Mobile Server
    Route::get('/mobile/get_buku','MobileControl@get_Buku');
    Route::get('/mobile/get_koleksi/{kd_buku}','MobileControl@get_Koleksi');
    Route::post('/mobile/registrasi','MobileControl@registrasi');
    Route::post('/mobile/login','MobileControl@login');
    Route::get('/mobile/get_pinjam/{status}/{no_anggota}','MobileControl@get_pinjam');
    Route::post('/mobile/save_booking','MobileControl@save_booking');
