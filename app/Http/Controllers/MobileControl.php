<?php
	
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use App\MBuku;
use App\User;
use App\MAnggota;
use Session; 

class MobileControl extends Controller
{
    //
	function get_Buku(){
		header('Access-Control-Allow-Origin: *');
		// Get Data Buku
		$buku = DB::select('SELECT tb_buku.*,tb_penerbit.nama_penerbit, tb_kategori.nama_kategori,tb_pengarang.nama_pengarang FROM  tb_buku left join tb_pengarang on tb_buku.kd_pengarang = tb_pengarang.kd_pengarang left join tb_penerbit on tb_buku.kd_penerbit = tb_penerbit.kd_penerbit left join tb_kategori on tb_buku.kd_kategori = tb_kategori.kd_kategori');

		// Mapping field cover 
		foreach($buku as $rsBuku){
			$rsBuku->cover=$rsBuku->cover==null ? asset('/img/no-cover.jpg') : asset('/img/'.$rsBuku->cover);
			$data[] = $rsBuku;
		}

		echo json_encode($data); // Nilai Balik
	}

	function get_Koleksi($kd_buku){
		header('Access-Control-Allow-Origin: *');

		$koleksi = DB::select('select * from tb_koleksi_buku WHERE kd_buku="'.$kd_buku.'"');

		echo json_encode($koleksi);
	}


	function registrasi(Request $req){
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Authorization, Content-Type' );

		$new = $req->json()->all(); // get data dari ionic

		$token = csrf_token();
		$user = new User([
			"name"=>$new['nama'],
			"alamat"=>$new['alamat'],
			"telp"=>$new['telp'],
			"email"=>$new['email'],
			"password"=>Hash::make($new['password']),
			"level"=>3,
			"remember_token"=>$token
		]);
		
		// Nilai balik
		if($user->save()){
			echo json_encode(["type"=>"success","msg"=>"Data Success Disimpan ! "]);			
		} else {
			echo json_encode(["type"=>"error","msg"=>"Data Gagal Disimpan ! "]);
		}
	}

	function login(Request $req){
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Authorization, Content-Type' );

		$login = $req->json()->all();

		// Cek Email
		$ceklogin = DB::table('users')->where('email',$login['email'])->first();
		if($ceklogin){
			if(Hash::check($login["password"],$ceklogin->password)){
				$profile = DB::select('SELECT tb_anggota.kd_anggota,users.email,users.level,tb_anggota.no_anggota,users.name,tb_anggota.tempat,tb_anggota.tgl_lahir,tb_anggota.jk,users.alamat,tb_anggota.kota,users.telp,tb_anggota.foto,tb_anggota.`status` from users left join tb_anggota on tb_anggota.email = users.email WHERE users.email="'.$login['email'].'"');
				echo json_encode(["type"=>"success","profile"=>$profile[0]]);
			} else {
				echo json_encode(["type"=>"error","msg"=>"Password Invalid !"]);
			}
		} else {
			echo json_encode(["type"=>"error","msg"=>"Email Invalid !"]);
		}

	}

	function get_pinjam($status,$no_anggota){
		header('Access-Control-Allow-Origin: *');
		
		$pinjam = DB::select('select no_pinjam,no_anggota,tgl_pinjam,tgl_kembali from tb_peminjaman where status = "'.$status.'" and no_anggota ="'.$no_anggota.'" group by no_pinjam,tgl_pinjam,tgl_kembali,no_anggota');
		echo json_encode($pinjam);
	}

	function save_member(Request $req){
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Authorization, Content-Type' );
		try {
			$data = $req->json()->all();
			if($data['no_anggota']==null){
				// Tambahkan Validasi buat sendiri

				// Menciptakaan kode anggota
				// A0001012019
				$newid = DB::select('SHOW TABLE STATUS LIKE "tb_anggota"');
				$noanggota = "A".sprintf('%04d',$newid[0]->Auto_increment).date("mY");

				// Simpan Ke Tabel Anggota
				$res = DB::table('tb_anggota')->insert([
					'no_anggota' => $noanggota,
					'nama' => $data['nama'],
					'tempat' => $data['tempat'],
					'tgl_lahir' => date("Y-m-d",strtotime($data['tgl_lahir'])),
					'jk' => $data['jk'],
					'alamat' => $data['alamat'],
					'kota' => $data['kota'],
					'telp' => $data['telp'],
					'email' => $data['email'],
					'status'=>1
				]);		

			} else {
				$res = DB::table('tb_anggota')->where("email",$data['email'])->update([
					'nama' => $data['nama'],
					'tempat' => $data['tempat'],
					'tgl_lahir' => date("Y-m-d",strtotime($data['tgl_lahir'])),
					'jk' => $data['jk'],
					'alamat' => $data['alamat'],
					'kota' => $data['kota'],
					'telp' => $data['telp']  
				]);	
			}

			$success = 1;
		} catch (\Exception $e){
			$success = 0;
		}
		
		if($success == 1){
			$profile = DB::select('SELECT tb_anggota.kd_anggota,users.email,users.level,tb_anggota.no_anggota,users.name,tb_anggota.tempat,tb_anggota.tgl_lahir,tb_anggota.jk,users.alamat,tb_anggota.kota,users.telp,tb_anggota.foto,tb_anggota.`status` from users left join tb_anggota on tb_anggota.email = users.email WHERE users.email="'.$data['email'].'"');

			// Save Base64 to Im
			$image = $data['foto'];  // your base64 encoded
			$image = str_replace('data:image/jpeg;base64,', '', $image);
			$image = str_replace(' ', '+', $image);
			$imageName = str_random(10).'.'.'png';
			\File::put(public_path(). '/img' . $imageName, base64_decode($image));

			echo json_encode(["type"=>"success","profile"=>$profile[0],"g"=>$data['foto']]);
		} else {
			echo json_encode(["type"=>"error","mess"=>"Error disimpan !"]);
		}		
	}

	function save_booking(Request $req){
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Authorization, Content-Type' );
		try {
			$newid = DB::select('SHOW TABLE STATUS LIKE "tb_peminjaman"');
			$no_pinjam = "P".sprintf('%06d',$newid[0]->Auto_increment); 
			
			// Mengambil data dari ionic
			$data = $req->json()->all();


			foreach($data['buku'] as $no_induk){    
				// Simpan ke tabel peminjaman      
				DB::table('tb_peminjaman')->insert([
					'no_pinjam'=>$no_pinjam,
					'tgl_pinjam'=>date('Y-m-d'),
					'tgl_kembali'=>date('Y-m-d',strtotime('+3 days')),
					'no_induk_buku'=>$no_induk['nib'],
					'no_anggota'=>$data['anggota'],
					'status'=>2
				]);
				
				// Update status buku jadi terpinjam
				DB::table('tb_koleksi_buku')->where("no_induk_buku",$no_induk['nib'])->update([
					'status'=>1
				]);
			}

			echo json_encode(["type"=>"success"]);
		} catch (\Exception $e){
			echo json_encode(["type"=>"error","mess"=>"Data Gagal disimpan !"]);
		}
		
	}
}
